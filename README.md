Role Nginx
=========

Роль для установки и настройки Nginx

Role Variables
--------------
#### Переменные установки/удаления пакетов:
  
      nginx_package_vers: ""
      nginx_package_state: "present"
      nginx_deinst_apache: false
  ___  
#### Переменные настройки конфигурационных файлов:
      
      nginx_remove_default: true
      nginx_config_name: "mysite.conf"
      nginx_config_mode: "0644"
  ___ 
#### Переменные конфига:
      
      nginx_servers_config: [{}]
  ___ 
  
Example Playbook
----------------

          - hosts: servers
            roles:
              - role: username.rolename
                vars:
                  nginx_servers_config:
                    - upstream: 
                        - name: "app-name"
                          zone: "exchange 64k"
                          ntlm: ""
                          server:
                            - "www.site.com"
                            - "www.site.ru"
                        - name: ...
                          ...
                      server_name: "localhost" or
                        - "site.com"
                        - "site.ru weight=5"
                      listen: "80" or
                        - "80"
                        - "88"
                      ssl:
                        certificate: "www.example.com.crt"
                        certificate_key: "www.example.com.key"
                        keepalive_timeout: "70"
                      root: "/var/www/html"
                      index: "index.html index.php"
                      links:
                        - location: "/app"
                          root: "/var/www/html"
                          index: "index.html index.php"
                          alias: "/var/www/html"
                          gzip:
                            "": "on"
                            types: "text/plain application/xml"
                            min_length: "1000"
                            proxied: "no-cache no-store private expired auth"
                          proxy: 
                            pass: ""
                            redirect: ""
                            bind: ""
                            set_header:
                              - Host "127.0.0.1"
                          allow: ""
                          deny: ""
                          links:
                            - location: ...
                              ...
License
-------

MIT

Author Information
------------------

vitoxaya
